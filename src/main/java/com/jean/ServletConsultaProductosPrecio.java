package com.jean;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.tools.ToolProvider;

import com.jean.entidad.EntidadProductos;


	
	



public class ServletConsultaProductosPrecio extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4414017327927356628L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		PrintWriter out= resp.getWriter();
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Conect");
	    EntityManager em= emf.createEntityManager();
	    
	    Boolean msj=false;
		
	    String precio= null ;
 
	  
 	try {
 		
 		precio= req.getParameter("precio");
 		
 		if (isNumeric(precio)){
 			
 			
 			List <EntidadProductos> listaProductos = em.createNamedQuery("EntidadProductos.consultaPorprecio",EntidadProductos.class).setParameter("miPrecio",Integer.valueOf(precio)).getResultList();
 			req.setAttribute("miLista", listaProductos);
 			msj=true;
 			
 			req.setAttribute("enviandoMsj", msj);
 		
 		}else{
 			if(precio==null){
 				
 				
 	 			List <EntidadProductos> listaProductos = em.createNamedQuery("EntidadProductos.consultaPorprecio",EntidadProductos.class).setParameter("miPrecio",0).getResultList();
 	 			req.setAttribute("miLista", listaProductos);
 	 			
 	 			msj=true;
 	 			req.setAttribute("enviandoMsj", msj);
 			}else{
 				
 				msj=false;
 	 			req.setAttribute("enviandoMsj", msj);
 			}
 			
 		}
	} catch (Exception e) {
		
		
		
	}
	    	
	    	
	    	
		      
			
			
		      
		       
		       
			req.setAttribute("EnviandoPrecio", precio);
			req.getRequestDispatcher("/ConsultarProductosPorPrecio.jsp").forward(req, resp);
	}
	
	
	private static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
}
	
}

