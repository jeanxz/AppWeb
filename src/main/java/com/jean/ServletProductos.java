package com.jean;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jean.entidad.EntidadProductos;

public class ServletProductos extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8688933111270077202L;

	
		
		
@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		


PrintWriter out = resp.getWriter();
		
		
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Conect");
	      EntityManager em= emf.createEntityManager();
	      
		List<EntidadProductos> miLista = em.createNamedQuery("EntidadProductos.mostraDatos",EntidadProductos.class).getResultList();
	      
		out.println("Mi Lista de productos");
		
		for(EntidadProductos lista:miLista){
			
			
			
			out.println("Nombre: "+lista.getNombre()+" Precio: "+lista.getPrecio()+"$");
			
		}
		out.close();
	

}
	
	
}
