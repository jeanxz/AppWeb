package com.jean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jean.entidad.EntidadAnimal;


public class MessageServerAnimales extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1912136079626179884L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		EntidadAnimal ani= new EntidadAnimal();
		
		ani.setNombre("Perro");
		ani.setEdad(3);
		
		List<EntidadAnimal> listaAnimales = new ArrayList<EntidadAnimal>();
		
		listaAnimales.add(new EntidadAnimal("Gato",3));
		listaAnimales.add(new EntidadAnimal("Gallo",8));
		listaAnimales.add(new EntidadAnimal("Teddy",14));
		listaAnimales.add(new EntidadAnimal("Caballo",20));
		
		req.setAttribute("perro", ani);
		req.setAttribute("miLista",listaAnimales);
		
		req.getRequestDispatcher("/informacion_Animales.jsp").forward(req, resp);
		
		
	}
	
	
	
	
}
