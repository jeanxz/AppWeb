package com.jean.entidad;

import java.io.Serializable;

public class EntidadAnimal implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7478641122154191300L;
	public EntidadAnimal(String nombre ,int edad) {
		this.setNombre(nombre);
		this.setEdad(edad);
	}
	
	public EntidadAnimal(){}
	
	private String nombre;
	private int edad;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	@Override
	public String toString() {
		return "EndidadAnimal [nombre=" + nombre + ", edad=" + edad + "]";
	}
	
	
}
